package com.aa.android.polcars;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

import org.ksoap2.serialization.SoapObject;

import android.os.Parcel;
import android.os.Parcelable;


public class PCarList implements Parcelable{

	public ArrayList<PCar> policeCarList;
	//public String toParse;
	
	public PCarList()	//inicjalizacja listy policyjnych samochodow przechowywanej w aplikacji
	{
		policeCarList = new ArrayList<PCar>();
	}
	
	public PCarList(Parcel in)	//inicjalizacja listy policyjnych samochodow przechowywanej w aplikacji
	{
		policeCarList = new ArrayList<PCar>();
		
	}
	
	public void lowerPriority(){
		//obni� priorytet ka�dego samochodu na li�cie o 1
		if(policeCarList.isEmpty()!=true){
			for(int k=0; k<policeCarList.size();k++)
			{
				policeCarList.get(k).priorytet = policeCarList.get(k).priorytet-1;
			}
		}
	}
	
	//funkcja wykrozystuj�ca klase do parsowania - wyci�ga poszczeg�lne samochody z XML, sprawdza czy juz istnieja w aplikacji i je dodaje/aktualizuje
	public String parseCars(SoapObject respon){
		String r = "";
		
		for(int i=0; i<respon.getPropertyCount();i++)
		{
			if (respon.getProperty(i) instanceof SoapObject) {parseCars((SoapObject) respon.getProperty(i));
		    } else {
		        if (i == respon.getPropertyCount() - 1) {
		        	PCar tempPC = new PCar();
		        	tempPC.nr_rej = respon.getProperty("nr_rej").toString();
		        	tempPC.model = respon.getProperty("model").toString();
		            tempPC.marka = respon.getProperty("marka").toString();
		            tempPC.kolor = respon.getProperty("kolor").toString();
		        	tempPC.wsp_x = respon.getProperty("wsp_x").toString();
		            tempPC.wsp_y = respon.getProperty("wsp_y").toString();
		            if(policeCarList.isEmpty()!=true){
			            for(int j = 0; j<policeCarList.size();j++)
			            {
			            	if(tempPC.nr_rej.equals(policeCarList.get(j).nr_rej) &&		//je�li taki samoch�d jest ju� w liscie to daj mu najwy�szy priorytet
			            			tempPC.model.equals(policeCarList.get(j).model) &&
			            			tempPC.marka.equals(policeCarList.get(j).marka) &&
			            			tempPC.kolor.equals(policeCarList.get(j).kolor))
			            	{
			            		policeCarList.remove(j);
			            	}
			            }
			            tempPC.priorytet = 5;
			            this.policeCarList.add(tempPC);		//jesli nie ma samochodu w aplikacji to go dodaj
		            }
		            else{
		            	tempPC.priorytet = 5;
		            	this.policeCarList.add(tempPC);		//jesli lista jest pusta to dodaj do niej samochod
		            }
		                     
		        }
		    }
		}
		
		if(policeCarList.isEmpty()!=true){
			for(int p=0; p<policeCarList.size();p++)
			{
				if(policeCarList.get(p).priorytet<=0)
				{
					policeCarList.remove(p);
				}
			}
		}
		
		
		return r;
	}//parseCars()

	public void clearAndSort() {
		//usu� samochody z priorytetem 0
		if(policeCarList.isEmpty()!=true){
			for(int p=0; p<policeCarList.size();p++)
			{
				if(policeCarList.get(p).priorytet<=0)
				{
					policeCarList.remove(p);
				}
			}
		}
		
		
		//sortuj liste zgodnie z priorytetami samochod�w
		
		Collections.sort(this.policeCarList, new Comparator<PCar>(){
			@Override public int compare(PCar p1, PCar p2){
				return p2.priorytet - p1.priorytet;
			}
			
		});
		
	}//clearAndSort

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(policeCarList);
		
	}

	

	
}

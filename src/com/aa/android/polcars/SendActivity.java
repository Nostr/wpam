package com.aa.android.polcars;


import com.example.android.location.R;

import android.support.v4.app.FragmentActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


@SuppressLint("NewApi")
public class SendActivity extends FragmentActivity {

	private String latX, lngY, nr_rej, mark, model, kolor;
	private TextView llText, errText;
	private EditText nrRej_editText, mark_editText, model_editText, kolor_editText;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send);
		android.app.ActionBar ab = getActionBar();
		ab.setDisplayHomeAsUpEnabled(false);
		
		Intent intent = getIntent();
		
		//pobierz pozycje przekazana z mainActivity
		String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		latX = message.substring(0, 5);
		lngY = message.substring(5);
		
		nrRej_editText = (EditText) findViewById(R.id.send_regnrEdit);
		mark_editText = (EditText) findViewById(R.id.send_markEdit);
		model_editText = (EditText) findViewById(R.id.send_modelEdit);
		kolor_editText = (EditText) findViewById(R.id.send_colorEdit);
 
		
		llText = (TextView) findViewById(R.id.send_latlngText);
		llText.setText("Wsp�rz�dne:   "+latX+"     "+lngY);

	}
	

// sprawdz wprowadzone dane i wyslij albo wyswietl inf o bledzie	
    public void checkAndSend(View view){
 
    	
    	nr_rej = nrRej_editText.getText().toString();
    	if(nr_rej.length() < 4){
    		errText = (TextView) findViewById(R.id.send_info);
    		errText.setText("B��dny numer rejestracyjny!");
    		return;
    	}
    	
    	mark = mark_editText.getText().toString();
    	if(mark.length() < 2){
    		errText = (TextView) findViewById(R.id.send_info);
    		errText.setText("B��dna marka!");
    		return;
    	}
    	
    	model = model_editText.getText().toString();
    	if(model.length() < 2){
    		errText = (TextView) findViewById(R.id.send_info);
    		errText.setText("B��dny model!");
    		return;
    	}
    	
    	kolor = kolor_editText.getText().toString();
    	if(kolor.length() < 2){
    		errText = (TextView) findViewById(R.id.send_info);
    		errText.setText("B��dny kolor!");
    		return;
    	}

    	Intent i = new Intent();

    	i.putExtra("EXTRA_REJST", nr_rej);
    	i.putExtra("EXTRA_MARKA", mark);
    	i.putExtra("EXTRA_MODEL", model);
    	i.putExtra("EXTRA_KOLOR", kolor);
    	i.putExtra("EXTRA_LATX", latX);
    	i.putExtra("EXTRA_LNGY", lngY);
    	this.setResult(RESULT_OK,i);
    	finish();
    }
    
    public void cancel(View view){
    	this.setResult(RESULT_CANCELED);
    	finish();
    }
    
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.send, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	



	

}

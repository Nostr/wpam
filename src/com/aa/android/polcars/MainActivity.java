/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.aa.android.polcars;


import com.example.android.location.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;



public class MainActivity extends Activity implements
        LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

	// sta�e do komunikacji z web service
	private static final String FIND_SOAP_ACTION = "http://tempuri.org/findCar";
	
	private static final String ADD_SOAP_ACTION = "http://tempuri.org/addCar";
	
	private static final String FIND_OPERATION_NAME ="findCar";
	
	private static final String ADD_OPERATION_NAME = "addCar";
	
	private static final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
	
	private static final String SOAP_ADDRESS = "http://aalaptop.noip.pl:8050/findCar/Service.asmx";
	
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	
    // zadanie polaczenia z usluga lokalizacyjna
    private LocationRequest mLocationRequest;

    // Przechowuje aktualna instancje location client
    private LocationClient mLocationClient;

    // Handles to UI widgets
    private TextView mTextLatLng;
    private TextView mTextInfo;
    public String car;						//do przekazywanie informacji
    public PCarList pCarList;				//lista pojazdow w pamieci aplikacji
    private double last_latX, last_lngY;	//ost znana pozycja
    private PCar carToSend;					//samochod ktory probujemy dodac do BD

    // Handle to SharedPreferences for this app
    SharedPreferences mPrefs;

    // Handle to a SharedPreferences editor
    SharedPreferences.Editor mEditor;

  //przechowuje informacj� o tym czy zatrzymana aktualizacje pozycji gps
    boolean mUpdatesRequested = false;

   
    /*
     * Inicjalizacja aktywno�ci
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);

	        // do wyswietlania wspolrzednych
	        mTextLatLng = (TextView) findViewById(R.id.lat_lng);
	        mTextInfo = (TextView)findViewById(R.id.text_WS1);	
	        
	        // nowa usluga lokalizacyjna
	        mLocationRequest = LocationRequest.create();
	
	        /*
	         * Ustaw cz�stotliwo�� aktualizacji (5 sekund)
	         */
	        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);
	
	        // ustaw wysoka dokladnosc
	        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	
	        // ustaw maksymalna szybkosc aktualizacji
	        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
	
	        // Open Shared Preferences
	        mPrefs = getSharedPreferences(LocationUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);
	
	        // Get an editor
	        mEditor = mPrefs.edit();
	
	        /*
	         * utworz klienta lokacji
	         */
	        mLocationClient = new LocationClient(this, this, this);
	        
	        //tw�rz now� liste, je�li nie ma �adnej        
	        if(pCarList==null){
	        	pCarList=new PCarList();
	        }
	        
	        last_latX = -10000;
	        last_lngY = -10000;
	        
	        
	        //odzyskiwanie danych z poprzedniego stanu
	        Object obj = getLastNonConfigurationInstance();
	        
	        if(savedInstanceState!= null)
	        {
	        	last_latX = savedInstanceState.getDouble("savedLastLatX");
	        	last_lngY = savedInstanceState.getDouble("savedLastLngY");
	        }

	        

        

    }
        
    //wypelnij list view
    private void populateListView(){
    	ArrayAdapter<PCar> adapter = new MyListAdapter();
    	ListView list = (ListView) findViewById(R.id.carsListView);
    	list.setAdapter(adapter);
    	
    }
    
    //adapter do zapelniania listy 
    private class MyListAdapter extends ArrayAdapter<PCar>{
    	public MyListAdapter(){
    		super(MainActivity.this, R.layout.item_v, pCarList.policeCarList);
    	}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			//czy mamy widok?
			View itemView = convertView;
			if(itemView == null)
			{
				itemView = getLayoutInflater().inflate(R.layout.item_v, null);
			}
			
			//pobierz pojazd
			PCar currCar = pCarList.policeCarList.get(position);
			
			//wypelnij widok
			TextView nr_rejText = (TextView) itemView.findViewById(R.id.item_textNrRej);
			nr_rejText.setText("Nr. Rej: "+currCar.nr_rej);
			nr_rejText.setTextColor(Color.rgb(3, 28, 199));
			
			TextView markaText = (TextView) itemView.findViewById(R.id.item_textMarka);
			markaText.setText(currCar.marka);
			
			TextView modelText = (TextView) itemView.findViewById(R.id.item_textModel);
			modelText.setText(currCar.model);
			
			
			TextView kolorText = (TextView) itemView.findViewById(R.id.item_textKolor);
			kolorText.setText(currCar.kolor);//+" "+currCar.priorytet);//kolorText.setText(currCar.kolor);
			
			//nadaj kolor zale�ny od priorytetu
			if(currCar.priorytet == 5)
			{
				itemView.setBackgroundColor(Color.rgb(255, 100, 100));
			}
			if(currCar.priorytet == 4)
			{
				itemView.setBackgroundColor(Color.YELLOW);
			}
			return itemView;
		}
    	
    	
    }
    
    /*
     *  Klasa do asynchronicznych zapytan do bazy danych przez Web Service
     */    
    private class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {          
            getWSData();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            populateListView();
            pCarList.clearAndSort();
            mTextInfo.setText(car);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }

    /*
     *  Klasa do asynchronicznej proby dodania pojazdu do bazy
     */    
    private class AsyncSendWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            putWSData();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("Vik", "onPostExecute");
            mTextInfo.setText(car);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }
    
    /*
     * Gdy aktywnosci zostanie zastoponwana
     */
    @Override
    public void onStop() {

        // jesli klient jest polaczony
        if (mLocationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        // rozlacz
        mLocationClient.disconnect();
       
        super.onStop();
    }
    
    /*
     * Przy pause
     */
    @Override
    public void onPause() {

        // Zachowaj aktualne ustawienia
        mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, mUpdatesRequested);
        mEditor.commit();

        super.onPause();
    }

    /*
     * Przy stacie
     */
    @Override
    public void onStart() {

        super.onStart();

        /*
         * Polacz klienta
         */
        mLocationClient.connect();

    }
    
    /*
     * Gdy aktywnosc jest widoczna
     */
    @Override
    public void onResume() {
        super.onResume();

            mUpdatesRequested = mPrefs.getBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);


            

    }

    /*
     *Os�uguje mi�dzy innymi rezultat wywo�ania przez u�ytkownika okna dodawania pojazdu do bazy danych.
     *R�wnie� obs�uguje problemyz  po��czeniem (mi�dzy innymi Google Play services)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

    	//jesli jest to powrot z dodawania samochodu
    	if(requestCode == 77)
    	{
    		if(resultCode == RESULT_OK)//i jest ok to wczytaj samochod do dodania i asynchronicznie wyslij go do WS
    		{
    			this.carToSend = new PCar();

    			
    			carToSend.nr_rej = intent.getStringExtra("EXTRA_REJST");
    			
    			carToSend.marka = intent.getStringExtra("EXTRA_MARKA");
    			carToSend.model = intent.getStringExtra("EXTRA_MODEL");
    			carToSend.kolor = intent.getStringExtra("EXTRA_KOLOR");
    			carToSend.wsp_x = intent.getStringExtra("EXTRA_LATX");
    			carToSend.wsp_y = intent.getStringExtra("EXTRA_LNGY");
    			
    			AsyncSendWS task = new AsyncSendWS();
    	    	task.execute();
    		}
    	}
    	
        // Choose what to do based on the request code
        switch (requestCode) {

            // If the request code matches the code sent in onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST :

                switch (resultCode) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:

                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.resolved));
                    break;

                    // If any other result was returned by Google Play services
                    default:
                        Log.d(LocationUtils.APPTAG, getString(R.string.no_resolution));

                    break;
                }

            // If any other request code was received
            default:
               // Report that this Activity received an unknown requestCode
               Log.d(LocationUtils.APPTAG,
                       getString(R.string.unknown_activity_request_code, requestCode));

               break;
        }
    }

    /**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates", "Google Play services is available.");

            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (dialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(dialog);
                //errorFragment.show(getSupportFragmentManager(), "Location Updates");
            }
            return false;
        }
    }


    //wywolanie nowej aktywnosci z ost znanymi wspolrzednymi w celu dodania pojazdu
    public void addCar(View v) {
    	if(last_latX!=-10000 || last_lngY!=-10000){
	    		
	    	Intent intent = new Intent(this, SendActivity.class);
	    	
			String latXString = String.valueOf(this.last_latX);
			String lngYString = String.valueOf(this.last_lngY);
			
			latXString = latXString.substring(0, 5);
			lngYString = lngYString.substring(0, 5);
			
			String message =latXString+lngYString;
			
	    	intent.putExtra(EXTRA_MESSAGE, message);
	    	intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivityForResult(intent, 77);
    	}
    	else{
    		mTextLatLng.setText(""+this.last_latX+" "+this.last_lngY+" bledne wsp");
    	}
    }

 
    /*
     *  wlacz sledzenie pozycji - wywolywane przez przycisk
     */
    public void startUpdates(View v) {
        mUpdatesRequested = true;

        if (servicesConnected()) {
            startPeriodicUpdates();
        }
        mTextInfo.setTag("Zaczynam");
    }

    /*
     *  zatrzymaj aktualizacje pozycji - wywolywane przez przycisk
     */
    public void stopUpdates(View v) {
        mUpdatesRequested = false;

        if (servicesConnected()) {
            stopPeriodicUpdates();
        }
        mTextInfo.setTag("Zatrzymuje");
    }
    
    //pobranie informacj przez WS o pojazdach w aktualnym sektorze
    public void getWSData(){
    		SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, FIND_OPERATION_NAME);
    		PropertyInfo propertyInfo = new PropertyInfo();
    		propertyInfo.type= PropertyInfo.STRING_CLASS;
    		propertyInfo.name = "eid";    		
    		
    		String latXString = String.valueOf(this.last_latX);
    		String lngYString = String.valueOf(this.last_lngY);
    		latXString = latXString.substring(0, 5);
    		lngYString = lngYString.substring(0, 5);

    		//dodaj wspolrzedne do zadania
    		request.addProperty("x_cor", latXString);
    		request.addProperty("y_cor", lngYString);
    		
    			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
    			envelope.dotNet = true;
    			
    			envelope.setOutputSoapObject(request);
    			
    			HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);	
    			
    			try{
    				httpTransport.call(FIND_SOAP_ACTION, envelope);
    				Object response = envelope.getResponse();
    				//obni� priorytet ka�dego samochodu na li�cie o 1
    				this.pCarList.lowerPriority();
    				//dodaje pobrane samochody do aplikacji
    				this.pCarList.parseCars((SoapObject)response);
    				car = "Pobrano:"+latXString+" "+lngYString;
    				//usuwa nieaktywne samochody i sortuje je
    				this.pCarList.clearAndSort();

    			}catch(Exception exception){

    				car = car + "Problem: "+latXString+" "+lngYString+" "+exception.toString();
    				//probuj wyslac zapytanie ponownie
    				getWSData();
    			}
    			
				
			
    }
    
    // wyslij dane do bazy danych przez WS
    private void putWSData(){
    	SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE, ADD_OPERATION_NAME);
		PropertyInfo propertyInfo = new PropertyInfo();
		propertyInfo.type= PropertyInfo.STRING_CLASS;
		propertyInfo.name = "eid";
	

		//dodaj informacje do zadania
		this.car = this.carToSend.nr_rej;
		request.addProperty("nr_rej", carToSend.nr_rej);
		request.addProperty("model", carToSend.model);
		request.addProperty("marka", carToSend.marka);
		request.addProperty("kolor", carToSend.kolor);
		request.addProperty("wsp_x", carToSend.wsp_x);
		request.addProperty("wsp_y", carToSend.wsp_y);
		
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			
			envelope.setOutputSoapObject(request);
			
			HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
			
			try{
				httpTransport.call(ADD_SOAP_ACTION, envelope);
				Object response = envelope.getResponse();
				car = "Dodano radiow�wz.";

			}catch(Exception exception){

				car = "Nie udane dodanie";
			}
			
			
    }
 

    /*
     * Wyswietl connected przy polaczeniu i ew zacznij aktualizowanie pozycji
     */
    @Override
    public void onConnected(Bundle bundle) {
    	Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();

        if (mUpdatesRequested) {
            startPeriodicUpdates();
        }
    }

    /*
     * wyswietl disconnected przy rozlaczeniu
     */
    @Override
    public void onDisconnected() {
    	Toast.makeText(this, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();

    }

    /*
     * Przy nieudanym polaczeniu.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {

                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */

            } catch (IntentSender.SendIntentException e) {

                // Log the error
                e.printStackTrace();
            }
        } else {

            // If no resolution is available, display a dialog to the user with the error.
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /*
	 *	Przy aktualizacji polozenia
     */
    @Override
    public void onLocationChanged(Location location) {

        double tempLatX, tempLngY;
        int tempIntLatX, tempIntLngY,IntLatX,IntLngY;
    	
        mTextLatLng.setText("Pobieranie pozycji");
        // jesli usluga jest dostepna
        if (servicesConnected()) {

            // odczytaj aktualna pozycje
            Location currentLocation = mLocationClient.getLastLocation();
            tempLatX=currentLocation.getLatitude();
            tempLngY=currentLocation.getLongitude();
            
            //obliczenia do porownania
            double riseUp = tempLatX*100;
            tempIntLatX = (int) riseUp;
            
            riseUp = tempLngY*100;
            tempIntLngY = (int) riseUp;
            
            riseUp = last_latX*100;
            IntLatX = (int) riseUp;
            
            riseUp = last_lngY*100;
            IntLngY = (int) riseUp; 
            		            

            
            //je�li przyebyli�my 00.01 w poziomie i/lub pionie - zapytaj baze danych o samochody i zaktualizuj aktualne po�o�enie
            if(Math.abs(tempIntLatX-IntLatX)>=1 || Math.abs(tempIntLngY-IntLngY)>=1)
            {
            	//stan pobrany ustaw jako ostatni znany i uruchom asynchroniczne zapytanie do bazy danych
            	last_latX = tempLatX;
            	last_lngY = tempLngY;
            	AsyncCallWS task = new AsyncCallWS();
            	task.execute();
            }
            
            // Wyswietl aktualna pozycje
            mTextLatLng.setText(LocationUtils.getLatLng(this, currentLocation));
        }
        else
        {
        	mTextLatLng.setText("service not connected");
        }
        
        
        
    }

    /*
     * Wyslij zadanie okresowej aktualizajci pozycji
     */
    private void startPeriodicUpdates() {

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
        mTextLatLng.setText("Pobieranie informacji");
            }

    /*
	 * zatrzymaj aktualizajce pozycji
     */
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
    }

    /**
     * An AsyncTask that calls getFromLocation() in the background.
     * The class uses the following generic types:
     * Location - A {@link android.location.Location} object containing the current location,
     *            passed as the input parameter to doInBackground()
     * Void     - indicates that progress units are not used by this subclass
     * String   - An address passed to onPostExecute()
     */
    
/*    protected class GetAddressTask extends AsyncTask<Location, Void, String> {

        // Store the context passed to the AsyncTask when the system instantiates it.
        Context localContext;

        // Constructor called by the system to instantiate the task
        public GetAddressTask(Context context) {

            // Required by the semantics of AsyncTask
            super();

            // Set a Context for the background task
            localContext = context;
        }

        /**
         * Get a geocoding service instance, pass latitude and longitude to it, format the returned
         * address, and return the address to the UI thread.
         */
/*        @Override
        protected String doInBackground(Location... params) {
            /*
             * Get a new geocoding service instance, set for localized addresses. This example uses
             * android.location.Geocoder, but other geocoders that conform to address standards
             * can also be used.
             */
/*            Geocoder geocoder = new Geocoder(localContext, Locale.getDefault());

            // Get the current location from the input parameter list
            Location location = params[0];

            // Create a list to contain the result address
            List <Address> addresses = null;

            // Try to get an address for the current location. Catch IO or network problems.
            try {

                /*
                 * Call the synchronous getFromLocation() method with the latitude and
                 * longitude of the current location. Return at most 1 address.
                 */
 /*               addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                // Catch network or other I/O problems.
                } catch (IOException exception1) {

                    // Log an error and return an error message
                    Log.e(LocationUtils.APPTAG, getString(R.string.IO_Exception_getFromLocation));

                    // print the stack trace
                    exception1.printStackTrace();

                    // Return an error message
                    return (getString(R.string.IO_Exception_getFromLocation));

                // Catch incorrect latitude or longitude values
                } catch (IllegalArgumentException exception2) {

                    // Construct a message containing the invalid arguments
                    String errorString = getString(
                            R.string.illegal_argument_exception,
                            location.getLatitude(),
                            location.getLongitude()
                    );
                    // Log the error and print the stack trace
                    Log.e(LocationUtils.APPTAG, errorString);
                    exception2.printStackTrace();

                    //
                    return errorString;
                }
                // If the reverse geocode returned an address
                if (addresses != null && addresses.size() > 0) {

                    // Get the first address
                    Address address = addresses.get(0);

                    // Format the first line of address
                    String addressText = getString(R.string.address_output_string,

                            // If there's a street address, add it
                            address.getMaxAddressLineIndex() > 0 ?
                                    address.getAddressLine(0) : "",

                            // Locality is usually a city
                            address.getLocality(),

                            // The country of the address
                            address.getCountryName()
                    );

                    // Return the text
                    return addressText;

                // If there aren't any addresses, post a message
                } else {
                  return getString(R.string.no_address_found);
                }
        }

        /**
         * A method that's called once doInBackground() completes. Set the text of the
         * UI element that displays the address. This method runs on the UI thread.
         */
/*        @Override
        protected void onPostExecute(String address) {

            // Turn off the progress bar
            //mActivityIndicator.setVisibility(View.GONE);

            // Set the address in the UI
            //mAddress.setText(address);
        }
    }
*/

    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
            errorCode,
            this,
            LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            //errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
        }
    }

    /**
     * Define a DialogFragment to display the error dialog generated in
     * showErrorDialog.
     */
    public static class ErrorDialogFragment extends DialogFragment {

        // Global field to contain the error dialog
        private Dialog mDialog;

        /**
         * Default constructor. Sets the dialog field to null
         */
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        /**
         * Set the dialog to display
         *
         * @param dialog An error dialog
         */
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        /*
         * This method must return a Dialog to the DialogFragment.
         */
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    
    //zapisywanie stanu    
    @Override
    public Object onRetainNonConfigurationInstance() {
        return pCarList;
    }
    


	@Override
	
	protected void onSaveInstanceState(Bundle state) {
	
	      super.onSaveInstanceState(state);  
	
	    state.putDouble("savedLastLatX", this.last_latX);
	    state.putDouble("savedLastLngY", this.last_lngY);
	
	}



    

}

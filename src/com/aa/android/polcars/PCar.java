package com.aa.android.polcars;

import android.os.Parcel;
import android.os.Parcelable;

public class PCar implements Parcelable{
	public String nr_rej;
	public String model;
	public String marka;
	public String kolor;
	public String wsp_x;
	public String wsp_y;
	public int priorytet;
	
	public PCar(){
		
	}
	
	public PCar(Parcel in){
		this();
		nr_rej = in.readString();
		model = in.readString();
		marka = in.readString();
		kolor = in.readString();
		wsp_x = in.readString();
		wsp_y = in.readString();
		priorytet = in.readInt();
	
	}
	
	int getPriorytet()
	{
		return priorytet;
	}
	
	public static final Parcelable.Creator<PCar> CREATOR = 
	        new Parcelable.Creator<PCar>() {

	            @Override
	            public PCar createFromParcel(Parcel source) {
	                return new PCar(source);
	            }

	            @Override
	            public PCar[] newArray(int size) {
	                return new PCar[size];
	            }
	        };

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(nr_rej);
		dest.writeString(model);
		dest.writeString(marka);
		dest.writeString(kolor);
		dest.writeString(wsp_x);
		dest.writeString(wsp_y);
		dest.writeInt(priorytet);
		
	}
}
